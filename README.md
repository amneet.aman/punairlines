This project contains 2 endpoints:
  a) GET /flights

  This provides all the flights which are scheduled for today.
  We can filter the flights with Airline code by providing query parameter in the URL
  e.g 
  /flights?airline=QF
  
  b) POST /flights

  This endpoint can be used for the saving the flight details.
  Sample Request:
  {
        "flightNumber": "QF401",
            "departurePort": "SYD",
            "arrivalPort": "MEL",
            "departureTime": "2021-07-17T09:00:23Z",
            "arrivalTime": "2021-07-17T10:25:23Z",
            "airline": "QF"
  }


 Build:
   System Requirements to run the project on local machine

   1. Java 8
   2. Java IDE
   3. Maven
   4. Postgress DB (update the DB details in application properties files.)

 Run the maven build command to create jar with below command.
  mvn clean install

Once jar is created , jar can be run as below command

  java -jar punairlines-0.0.1-SNAPSHOT.jar

  Production deployment:

  We can use maven commands to build this project and executable jars.
  
  Executable jar file can be containerised in a docker and docker image can be deployed to Kubernetes.



