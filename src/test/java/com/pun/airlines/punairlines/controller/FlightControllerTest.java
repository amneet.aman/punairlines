package com.pun.airlines.punairlines.controller;

import com.pun.airlines.punairlines.entities.Flights;
import com.pun.airlines.punairlines.model.FlightDetails;
import com.pun.airlines.punairlines.model.Response.FlightResponse;
import com.pun.airlines.punairlines.service.FlightService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;

@RunWith(MockitoJUnitRunner.class)
public class FlightControllerTest {

    @Mock
    FlightService flightService;

    FlightController controller;

    @Before
    public void setUp() {
        controller=new FlightController(flightService);
    }

    @Test
    public void getFlightWithoutAirlineTest() throws Exception {
        List<FlightDetails> flightList= new ArrayList<>();

        FlightDetails flightDetails=FlightDetails.builder()
                .flightNumber("QF400").build();
        flightList.add(flightDetails);
        FlightResponse flightResponse=FlightResponse.builder()
                .flights(flightList).build();
        Mockito.when(flightService.getFlight(null)).thenReturn(flightResponse);
        ResponseEntity responseEntity = controller.getFlights(null);
        Assert.assertNotNull(flightResponse);
        Assert.assertEquals(200,responseEntity.getStatusCodeValue());
    }

    @Test
    public void getFlightWithAirlineTest() throws Exception {
        List<FlightDetails> flightList= new ArrayList<>();

        FlightDetails flightDetails=FlightDetails.builder()
                .flightNumber("QF400").build();
        flightList.add(flightDetails);
        FlightResponse flightResponse=FlightResponse.builder()
                .flights(flightList).build();
        Mockito.when(flightService.getFlight(anyString())).thenReturn(flightResponse);
        ResponseEntity responseEntity = controller.getFlights("QF");
        Assert.assertNotNull(flightResponse);
        Assert.assertEquals(200,responseEntity.getStatusCodeValue());
    }

    @Test
    public void getFlightWithNullResponseTest() throws Exception {
        List<FlightDetails> flightList= new ArrayList<>();

        FlightDetails flightDetails=FlightDetails.builder()
                .flightNumber("QF400").build();
        flightList.add(flightDetails);
        FlightResponse flightResponse=FlightResponse.builder().build();
        Mockito.when(flightService.getFlight("QF")).thenReturn(flightResponse);
        ResponseEntity responseEntity = controller.getFlights("QF");
        Assert.assertNotNull(flightResponse);
        Assert.assertEquals(404,responseEntity.getStatusCodeValue());
    }

    @Test
    public void saveFlightTest() throws Exception {

        FlightDetails flightDetails=FlightDetails.builder()
                .flightNumber("QF400").build();
        FlightResponse flightResponse=FlightResponse.builder().build();
        Mockito.when(flightService.saveFlight(any())).thenThrow(new Exception());
        ResponseEntity responseEntity = controller.saveFlights(flightDetails);
        Assert.assertEquals(500,responseEntity.getStatusCodeValue());
    }

    @Test
    public void saveFlightErrorTest() throws Exception {

        FlightDetails flightDetails=FlightDetails.builder()
                .flightNumber("QF400").build();
        FlightResponse flightResponse=FlightResponse.builder().build();
        Mockito.when(flightService.saveFlight(any())).thenReturn(flightDetails);
        ResponseEntity responseEntity = controller.saveFlights(flightDetails);
        Assert.assertEquals(200,responseEntity.getStatusCodeValue());
    }

    @Test
    public void getFlightWithExceptionResponseTest() throws Exception {
        List<FlightDetails> flightList= new ArrayList<>();

        FlightDetails flightDetails=FlightDetails.builder()
                .flightNumber("QF400").build();
        flightList.add(flightDetails);
        FlightResponse flightResponse=FlightResponse.builder().build();
        Mockito.when(flightService.getFlight("QF")).thenThrow(new Exception());
        ResponseEntity responseEntity = controller.getFlights("QF");
        Assert.assertNotNull(flightResponse);
        Assert.assertEquals(500,responseEntity.getStatusCodeValue());
    }
}
