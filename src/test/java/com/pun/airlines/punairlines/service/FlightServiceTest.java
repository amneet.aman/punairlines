package com.pun.airlines.punairlines.service;

import com.pun.airlines.punairlines.entities.Flights;
import com.pun.airlines.punairlines.model.FlightDetails;
import com.pun.airlines.punairlines.model.Response.FlightResponse;
import com.pun.airlines.punairlines.repositories.FlightsRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;


import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class FlightServiceTest {
    @Mock
    FlightsRepository flightsRepository;

    private FlightService flightService;

    @Before
    public void setUp() {
     flightService=new FlightService(flightsRepository);
    }

    @Test
    public void getFlightWithoutAirlineTest() throws Exception {
        List<Flights> flightList= new ArrayList<>();
        Flights flight = new Flights();
        flight.setAirline("QF");
        flight.setFlightNumber("QF888");
        flight.setArrivalPort("SYD");
        flight.setDeparturePort("MEL");
        flightList.add(flight);
        Mockito.when(flightsRepository.findByDepartureTimeBetween(any(),any())).thenReturn(flightList);
        FlightResponse flightResponse=flightService.getFlight(null);
        Assert.assertNotNull(flightResponse);
        Assert.assertEquals(1,flightResponse.getFlights().size());
    }

    @Test
    public void getFlightWithAirlineTest() throws Exception {
        List<Flights> flightList= new ArrayList<>();
        Flights flight = new Flights();
        flight.setAirline("QF");
        flight.setFlightNumber("QF888");
        flight.setArrivalPort("SYD");
        flight.setDeparturePort("MEL");
        flightList.add(flight);
        Mockito.when(flightsRepository.findByDepartureTimeBetweenAndAirline(any(),any(),anyString())).thenReturn(flightList);
        FlightResponse flightResponse=flightService.getFlight("QF");
        Assert.assertNotNull(flightResponse);
        Assert.assertEquals(1,flightResponse.getFlights().size());
    }

    @Test
    public void saveFlightTest() throws Exception {
        Flights flight = new Flights();
        flight.setAirline("QF");
        flight.setFlightNumber("QF888");
        flight.setArrivalPort("SYD");
        flight.setDeparturePort("MEL");
        flight.setId(new Long(1));
        Mockito.when(flightsRepository.save(any())).thenReturn(flight);
        FlightDetails flightDetails = FlightDetails.builder().build();
        flightDetails =flightService.saveFlight(flightDetails);
        Assert.assertNotNull(flightDetails);
        Assert.assertEquals(new Long(1),flightDetails.getId());
    }
}
