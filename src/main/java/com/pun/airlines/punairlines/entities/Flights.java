package com.pun.airlines.punairlines.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;




@Entity
@NoArgsConstructor
@Getter
@Setter
public class Flights {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;
    private String flightNumber;
    private String departurePort;
    private  String arrivalPort;
    @Temporal(TemporalType.TIMESTAMP)
    private Date departureTime;
    @Temporal(TemporalType.TIMESTAMP)
    private Date arrivalTime;
    private String airline;
}
