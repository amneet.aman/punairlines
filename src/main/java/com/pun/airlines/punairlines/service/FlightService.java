package com.pun.airlines.punairlines.service;

import com.pun.airlines.punairlines.controller.FlightController;
import com.pun.airlines.punairlines.entities.Flights;
import com.pun.airlines.punairlines.model.FlightDetails;
import com.pun.airlines.punairlines.model.Response.FlightResponse;
import com.pun.airlines.punairlines.repositories.FlightsRepository;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


@Service
@AllArgsConstructor
public class FlightService {

    public static  Logger logger = LoggerFactory.getLogger(FlightService.class);
    FlightsRepository flightsRepository;

    public FlightResponse getFlight(String airline) throws Exception {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        cal.set(Calendar.HOUR_OF_DAY,0);
        cal.set(Calendar.SECOND,0);
        cal.set(Calendar.MINUTE,0);
        cal.set(Calendar.MILLISECOND,0);
        Date startDate= formatter.parse(formatter.format(cal.getTime()));
        cal.set(Calendar.HOUR_OF_DAY,23);
        cal.set(Calendar.SECOND,59);
        cal.set(Calendar.MINUTE,59);
        cal.set(Calendar.MILLISECOND,59);
        Date endDate= formatter.parse(formatter.format(cal.getTime()));;
        List<Flights> flights=null;
        if(airline==null) {
            flights = flightsRepository.findByDepartureTimeBetween(startDate, endDate);
        }
        else {
            flights = flightsRepository.findByDepartureTimeBetweenAndAirline(startDate, endDate, airline);
        }
        logger.debug("Response from the Database {}", flights.toString());
        ArrayList flightList= new ArrayList();
        for(Flights flight: flights)
       {
             FlightDetails flightDetails=FlightDetails.builder()
                 .airline(flight.getAirline())
                 .arrivalPort(flight.getArrivalPort())
                 .departurePort(flight.getDeparturePort())
                 .arrivalTime(flight.getArrivalTime())
                 .departureTime(flight.getDepartureTime())
                 .flightNumber(flight.getFlightNumber())
                 .build();
      flightList.add(flightDetails);
       }

        return FlightResponse.builder().flights(flightList).build();
    }

    public FlightDetails saveFlight(FlightDetails flightDetails) throws Exception {
        Flights flight=new Flights();
        flight.setFlightNumber(flightDetails.getFlightNumber());
                flight.setAirline(flightDetails.getAirline());
        flight.setArrivalPort(flightDetails.getArrivalPort());
        flight.setDeparturePort(flightDetails.getDeparturePort());
        flight.setArrivalTime(flightDetails.getArrivalTime());
        flight.setDepartureTime(flightDetails.getDepartureTime());
        flight= flightsRepository.save(flight);
        logger.debug("Response  saved successfully to Database");
        flightDetails.setId(flight.getId());
        return flightDetails;
    }
}
