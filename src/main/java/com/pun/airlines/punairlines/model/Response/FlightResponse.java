package com.pun.airlines.punairlines.model.Response;

import com.pun.airlines.punairlines.model.FlightDetails;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@Builder
public class FlightResponse {

    private List<FlightDetails> flights;
}
