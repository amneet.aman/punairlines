package com.pun.airlines.punairlines.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.lang.NonNull;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Getter
@Setter
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FlightDetails {

    @NotBlank(message="Flight Number is mandatory")
    private String flightNumber;

    @NotBlank(message="departurePort is mandatory")
    private String departurePort;

    @NotBlank(message="arrivalPort is mandatory")
    private  String arrivalPort;
    private Long Id;

    @NotNull(message="departureTime is mandatory")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private Date departureTime;

    @NotNull(message="arrivalTime is mandatory")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private Date arrivalTime;

    @NotBlank(message="Airline is mandatory")
    private String airline;
}
