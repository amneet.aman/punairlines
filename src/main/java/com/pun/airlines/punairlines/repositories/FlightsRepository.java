package com.pun.airlines.punairlines.repositories;

import com.pun.airlines.punairlines.entities.Flights;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.Date;
import java.util.List;


public interface FlightsRepository extends JpaRepository<Flights,Long> {

    List<Flights> findByDepartureTimeBetween(Date start, Date end);
    List<Flights> findByDepartureTimeBetweenAndAirline(Date start, Date end,String airline);
}
