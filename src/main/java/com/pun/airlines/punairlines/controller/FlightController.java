package com.pun.airlines.punairlines.controller;

import com.pun.airlines.punairlines.model.FlightDetails;
import com.pun.airlines.punairlines.model.Response.FlightResponse;

import com.pun.airlines.punairlines.service.FlightService;

import lombok.AllArgsConstructor;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.text.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@Controller
@AllArgsConstructor
@Validated
public class FlightController {

    private FlightService flightService;
   public static Logger logger = LoggerFactory.getLogger(FlightController.class);

    @RequestMapping(value="/flights", method= RequestMethod.GET, produces = "application/json")
    public ResponseEntity getFlights(@RequestParam(required = false) String airline) {
        logger.debug("Get request for flights with Airline {}",airline);
        try {
            FlightResponse flightDetails = flightService.getFlight(airline);
            logger.debug("Response for get flight request {}", flightDetails.toString());
            if (flightDetails != null && flightDetails.getFlights() != null && flightDetails.getFlights().size() > 0)
                return new ResponseEntity(flightDetails, HttpStatus.OK);
            else
                return new ResponseEntity(HttpStatus.NOT_FOUND);
        }catch(Exception e)
        {
            logger.error("Error occurred while getting flight details", e.getMessage());
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value="/flights", method= RequestMethod.POST, produces = "application/json")
    public ResponseEntity saveFlights(@Valid @RequestBody FlightDetails flight)
    {
        try {
            logger.debug("Save request for flights {}", flight.toString());
            flight = flightService.saveFlight(flight);
            return new ResponseEntity(flight, HttpStatus.OK);
        }
        catch(Exception e)
        {
            logger.error("Error occurred while saving flight details", e.getMessage());
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
