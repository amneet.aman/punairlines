package com.pun.airlines.punairlines;

import com.pun.airlines.punairlines.controller.FlightController;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableAutoConfiguration
public class PunairlinesApplication {

	public static void main(String[] args) {
		SpringApplication.run(PunairlinesApplication.class, args);
	}

}
